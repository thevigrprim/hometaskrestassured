import io.restassured.response.Response;
import io.restassured.response.ValidatableResponse;
import org.testng.annotations.Test;
import pojo.*;

import static io.restassured.RestAssured.*;
import static org.assertj.core.api.Assertions.assertThat;

public class EndpoitsTests {
    private final static String URL = "https://reqres.in/";

    @Test
    public void singleUserNameTest() {
        String reqName = "Janet";

        UserData user = when()
                .get(URL + "api/users/2")
                .jsonPath()
                .getObject("data",UserData.class);

        assertThat(user.getFirstName()).contains(reqName);
    }
    @Test
    public void updateUserNameTest(){
        UserInfo info = new UserInfo();
        info.setName("morpheus");
        info.setJob("zion resident");

        UserPutResponse userPutResponse = given()
                .baseUri(URL)
                .basePath("api/users/2")
                .body(info)
                .when().put()
                .then().extract().as(UserPutResponse.class);

        assertThat(userPutResponse.getName())
                .contains("morpheus");
    }

    @Test
    public void successfulRegisterTest(){
        RegisterData registerData = RegisterData.builder()
                .email("eve.holt@reqres.in").password("pistol")
                .build();

        SuccessfulRegister successfulRegister = given()
                .baseUri(URL)
                .body(registerData)
                .when().post("api/register")
                .then().extract().as(SuccessfulRegister.class);
        System.out.println(successfulRegister.getToken());
        assertThat(successfulRegister.getToken()).contains("QpwL5tke4Pnpja7X4");
    }

    @Test
    public void deleteTest(){
        ValidatableResponse response = given()
                .baseUri(URL)
                .when()
                .delete("api/users/2")
                .then().statusCode(204);
    }

}
